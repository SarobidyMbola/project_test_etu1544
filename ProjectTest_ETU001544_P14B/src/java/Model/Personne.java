/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import utilitaire.*;
/**
 *
 * @author hhyy
 */
public class Personne {
    private String nom = null;
    private int age = 0;

    public Personne(){

    }
              
    @MethodeFile(name="SavePers")
    public ModelView save(){
       ModelView model = new ModelView();
       model.setAttribute("pers",(Personne)this);
       model.setFileJsp("reponseForm.jsp");
       return model;
    }
            
    @MethodeFile(name="miteny")
    public ModelView Miteny(){
       ModelView model = new ModelView();
       int a = 1;
       double d = 15.0;
       String feo = "miteny";
       model.setAttribute("a", a);
       model.setAttribute("feo", feo);
       model.setAttribute("double", d);
       model.setFileJsp("Test.jsp");
       return model;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    
}
