<%-- 
    Document   : Test.jsp
    Created on : 9 nov. 2022, 21:04:10
    Author     : hhyy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    int a = (int)request.getAttribute("a");
    double d = (double)request.getAttribute("double");
    String feo = (String)request.getAttribute("feo");
    %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>a : <%=a %></h1>
        <h1> feo <%=feo %> </h1>
        <p>double : <%=d %></p>
    </body>
</html>
